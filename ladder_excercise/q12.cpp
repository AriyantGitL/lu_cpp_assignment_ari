#include<iostream>
using namespace std;

int main()
{
    int x,y;
    cout<<"Enter x: ";
    cin>>x;
    cout<<"Enter y: ";
    cin>>y;
    if(x==0 && y==0)
        cout<<"X & Y are in origin";
    else if(x==0 && y!=0)
        cout<<"X & Y are in y-axis";
    else if(x!=0 && y==0)
        cout<<"X & Y are in x-axis";
    else
        cout<<"X & Y are neither in x-axis  nor in y-axis";
    return 0;
}