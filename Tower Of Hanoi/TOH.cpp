#include<iostream>
using namespace std;
void TOH(int ,char,char,char );
int main()
{
    int n=4;
    TOH(n,'A','B','C');
    return 0;
}
void TOH(int n, char source,char destination,char temp)
{
    if (n == 1)
    {
       // printf("\n Move disk 1 from rod %c to rod %c", source, destination);

        cout<<"\n Move disk 1 from rod "<<source<<" to rod "<<destination;
        return;
    }
    TOH(n-1, source, temp, destination);
    cout<<"\n Move disk "<<n<<" from rod "<<source<<" to rod "<<destination;
    //printf("\n Move disk %d from rod %c to rod %c", n, source, destination);
    TOH(n-1, temp, destination, source);
}
