#include<iostream>
using namespace std;
int sumofarray(int *,int);
int main()
{
    int *a,n;
    cout<<"Enter number of element in array:";
    cin>>n;
    a=new int(n);
    int sum=0;
    for(int i=0;i<n;i++)
    {
        cout<<"\nENter data:";
        cin>>a[i];
        sum+=a[i];
    }
    for(int i=0;i<n;i++)
    {
        cout<<"\t"<<a[i];
        
    }
    cout<<"\nSum of Array in main function:"<<sum;
    cout<<endl<<"\nSum of Array in User defined function:"<<sumofarray(a,n);
    return 0;
}
int sumofarray(int *a,int n)
{
    int sum=0;
    for(int i=0;i<n;i++)
    {
        sum+=a[i];
    }
    return sum;
}