#include <iostream>
#include <typeinfo>
using namespace std;

int main()
{
    int a[5] = {4, 8, 11, 12, 4}, n = 5;
    for (int i = 0; i < n; i++)
    {
        cout << endl
             << &a[i];
    }
    /*a[2], *a+2, a+2, *(a+2)*/
    cout << endl<< a[2];
    cout << endl<< *a + 2;
    cout << endl<< a + 2;
    cout<<endl<<*(a+2);
    return 0;
}