#include <iostream>
using namespace std;
struct Node
{
    int data;
    struct Node *next;
    /* data */
};

typedef struct Node Node;
void create(Node *);
void insertatFirst(Node *);
void insertatEnd(Node *);
void disp(Node *);
void insertatposition(Node *);
int getSize(Node *);
void deleteatfirst();
void deleteatend();
void deleteatposition();
void reverse();
Node *head;
int main()
{
    int ch;
    head = NULL;
    do
    {
        cout << endl
             << "MENU FOR LINKLIST" << endl;
        cout << endl
             << "1  CREATE ";
        cout << endl
             << "2  INSERT AT FIRST ";
        cout << endl
             << "4  INSERT at end ";
        cout << endl
             << "5  INSERT at Any position ";
        cout << endl
             << "3  DISPLAY ";
        cout << endl
             << "6  Delete from first ";
        cout << endl
             << "7  Delete from END ";
        cout << endl
             << "8  Delete from ANY POSITION ";
        cout << endl
             << "9  REVERSE THE LINKED LIST ";
        cout << endl
             << endl
             << "ENTER YOUR CHOICE :";

        cin >> ch;

        switch (ch)
        {
        case 1:
            create(head);
            break;
        case 2:
            insertatFirst(head);
            break;
        case 3:
            disp(head);
            break;
        case 4:
            insertatEnd(head);
            break;
        case 5:
            insertatposition(head);
            break;
        case 6:
            deleteatfirst();
            break;
        case 7:
            deleteatend();
            break;
        case 8:
            deleteatposition();
            break;
        case 9:
            reverse();
            break;
        }
    } while (ch >= 1 && ch <= 10);
    return 0;
}
void create(Node *nn)
{
    if (nn == NULL)
    {
        Node *newNode = new Node;
        cout << "\nEnter data:";
        cin >> newNode->data;
        newNode->next = head;
        head = newNode;
        cout << "\n"
             << newNode->data << " is inserted into list";
    }
    else
        cout << "Creation Already done";
}
void insertatFirst(Node *nn)
{
    Node *newNode = new Node;
    cout << "\nEnter data:";
    cin >> newNode->data;
    newNode->next = head;
    head = newNode;
    cout << "\n"
         << newNode->data << " is inserted into list";
}

void disp(Node *ptr)
{
    cout << endl;
    while (ptr != NULL)
    {
        cout << ptr->data << "->";
        ptr = ptr->next;
    }
    cout << "NULL";
}
void insertatEnd(Node *ptr)
{
    if (ptr == NULL)
    {
        insertatFirst(ptr);
    }
    else
    {
        Node *newNode = new Node;
        cout << "\nEnter data:";
        cin >> newNode->data;
        newNode->next = NULL;

        Node *temp = ptr;
        while (temp->next != NULL)
        {
            temp = temp->next;
        }
        temp->next = newNode;
        cout << "\n"
             << newNode->data << " is inserted into list";
    }
}
void insertatposition(Node *ptr)
{
    int pos;
    cout << "Enter the position to add new node:";
    cin >> pos;

    if (pos == 1)
        insertatFirst(ptr);
    else if (pos < 1)
        cout << "Please correct the position";
    else
    {

        if (pos >= 2 && pos <= getSize(ptr))
        {
            Node *newNode = new Node();
            cout << "\nEnter data:";
            cin >> newNode->data;
            Node *temp = head;

            for (int i = 2; i < pos; i++)
            {
                if (temp->next != NULL)
                {
                    temp = temp->next;
                }
            }
            newNode->next = temp->next;
            temp->next = newNode;
            cout << "\n"
                 << newNode->data << " is inserted into list";
        }
        else if (pos > getSize(ptr))
        {
            insertatEnd(ptr);
        }
    }
}

int getSize(Node *nn)
{
    if (nn == NULL)
        return 0;
    else
    {
        int size = 0;
        while (nn != NULL)
        {
            size++;
            nn = nn->next;
        }
        return size;
    }
}

void deleteatfirst()
{
    if (head == NULL)
        cout << "No node is found in link list";
    else
    {
        Node *temp = head;
        cout << "\n"
             << head->data << " is deleted from list.";
        head = head->next;
        delete (temp);
    }
}

void deleteatend()
{
    if (head == NULL)
        cout << "\n\nNo Node is found in list ";
    else
    {

        Node *temp = head;
        Node *ptr = head->next;
        if (temp == NULL)
            cout << "\nNo NODE FOUND IN LIST";
        else if (ptr == NULL)
        {
            head = NULL;
            delete (ptr);
        }
        else
        {
            while (ptr->next != NULL)
            {
                temp = temp->next;
                ptr = ptr->next;
            }
            temp->next = NULL;
            cout << "\n Deleted item is:" << ptr->data;
            delete (ptr);
        }
    }
}
void deleteatposition()
{
    int pos;
    cout << "\nENter data position to delete";
    cout<<getSize(head);
    if (pos < 1 || pos > getSize(head))
        cout << "Delete not possible";
    else
    {
        cout<<"\n\n\nelse part";
        Node *temp = head;
        Node *ptr = head->next;
        for (int i = 0; i < pos-1; i++)
        {
            temp = temp->next;
            ptr = ptr->next;
        }
        temp->next = ptr->next;
        delete (ptr);
    }
}
void reverse()
{
    Node *prev=NULL;
    Node *next=NULL;
    while(head!=NULL)
    {
        next=head->next;
        head->next=prev;
        prev=head;
        head=next;
    }
    head=prev;
}