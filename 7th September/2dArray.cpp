#include <iostream>
using namespace std;

int main()
{
    int a[5][5], b[5][5], r = 2, c = 2;
    cout << endl
         << "Enter data for 1st Matrix";
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            cout << endl
                 << "Emter Data:";
            cin >> a[i][j];
        }
    }
    cout << endl
         << "Enter data for 2nd Matrix";
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            cout << endl
                 << "Emter Data:";
            cin >> b[i][j];
        }
    }

    cout << endl
         << "Display data for 1st Matrix" << endl;
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            cout << a[i][j] << "\t";
        }
        cout << endl;
    }
    cout << endl
         << "Display data for 2nd Matrix" << endl;
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            cout << b[i][j] << "\t";
        }
        cout << endl;
    }
    cout << endl
         << "Addition of 1st and 2nd Matrix" << endl;
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            cout << b[i][j] + a[i][j] << "\t";
        }
        cout << endl;
    }

    int mul[10][10];
    for (int i = 0; i < r; i++)
    {
        for (int j = 0; j < c; j++)
        {
            mul[i][j] = 0;
            for (int k = 0; k < c; k++)
            {
                mul[i][j] += a[i][k] * b[k][j];
            }
        }
    }

    cout << endl
         << "Multiplication of 1st and 2nd Matrix" << endl;
    for (int i = 0; i < c; i++)
    {
        for (int j = 0; j < r; j++)
        {
            cout << mul[i][j] << "\t";
        }
        cout << endl;
    }
    return 0;
}