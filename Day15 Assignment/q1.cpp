#include<iostream>
using namespace std;

int main()
{
    int num[10],pos=0,neg=0,zero=0,even=0,odd=0;
    for(int i=0;i<10;i++)
    {
        cout<<endl<<"Enter "<<i+1<<"th position number: ";
        cin>>num[i];
        if(num[i]>0)
        {
            pos+=1;
            if(num[i]%2==0)
                even+=1;
            else if(num[i]%2!=0)
                odd+=1;
        }
        else if(num[i]<0)
        {
            neg+=1;
            if(num[i]%2==0)
                even+=1;
            else if(num[i]%2!=0)
                 odd+=1;
        }
        else if(num[i]==0)
            zero+=1;
    }
    for(int i=0;i<10;i++)
    {
        cout<<num[i]<<"\t";
    }
    cout<<endl<<"Total no of positive number:"<<pos;
    cout<<endl<<"Total no of negative number:"<<neg;
    cout<<endl<<"Total no of even number:"<<even;
    cout<<endl<<"Total no of odd number:"<<odd;
    cout<<endl<<"Total no of zeros:"<<zero;
    
    return 0;
}