#include<iostream>
using namespace std;

struct Queue
{
    int rear,front,q[5],size=5;
    /* data */
}q;

typedef struct Queue Queue;
    
void enqueue();
void dequeue();
void search();
void disp();
void swap();
int lsearch(int);
int main()
{
    q.front=-1;
    q.rear=-1;
    int ch;
    do
    {
       cout<<endl<<endl<<"MENU FOR QUEUE";
       cout<<endl<< "1.  enqueue";
       cout<<endl<< "2.  dequeue";
       cout<<endl<< "3.  search";
       cout<<endl<< "4.  display";
       cout<<endl<< "5.  Swap data";
       cout<<endl<<endl<<"ENTER YOUR CHOICE :";
       cin>>ch;

       switch(ch)
       {
            case 1:
                    enqueue();
                    break;
            case 2:
                    dequeue();
                    break;
            case 3:
                    search();
                    break;
            case 4:
                    disp();
                    break;
            case 5:
                    swap();
                    break;
                    
       }
    } while (ch>=1&&ch<=5);
    return 0;
}
void enqueue()
{
    if(q.rear==q.size-1)
    {
        cout<<"\n Queue is full";
    }
    else
    {
        if(q.rear==-1 && q.front==-1)
            {
                q.rear++;
                q.front++;
            }
        else
            {
                q.rear++;
            }
            cout<<"\n Enter the data:";
            cin>>q.q[q.rear];
            cout<<"\n "<<q.q[q.rear]<<" is inserted into queue";
    }
    

}
void dequeue()
{
    if(q.front==-1 && q.rear==-1)
        cout<<"\n Queue is empty";
    else
    {
        cout<<"\n"<<q.q[q.front]<<" is dequeue from Queue";
        if(q.rear==q.front)
        {
            q.rear=-1;
            q.front=-1;
        }
        else
            q.front++;
    }
}
void disp()
{
    
    if(q.front==-1 && q.rear==-1)
        cout<<"\n Queue is empty";
    else
    {
        for(int i=q.front;i<=q.rear;i++)
        {
            cout<<"\nQueue item:"<<q.q[i];
        }
    }
}
void search()
{
    int sd,f=0;
    cout<<endl<<"Enter searching data:";
    cin>>sd;
    for(int i=q.front;i<=q.rear;i++)
    {
        if(sd==q.q[i])
        {
            cout<<"Data is found at position:"<<i;
            f=1;
            break;
        }
            
    }
    if(f==0)
        cout<<endl<<sd<<" Data is not in queue";
}

void swap()
{
    int a,b,t;
    cout<<endl<<"Enter first element to swap:";
    cin>>a;
    cout<<endl<<"Enter 2nd element to swap with 1st element";
    cin>>b;
    int ai=lsearch(a);
    int bi=lsearch(b);
    if(ai==-1 || bi==-1)
    {
        cout<<"\nSwap is not possible because swaping data is not found in QUEUE."<<lsearch(a)<<"  "<<lsearch(b);
    }
    else
    {
        
        t=q.q[ai];
        q.q[ai]=q.q[bi];
        q.q[bi]=t;

    }
}
int lsearch(int sd)
{
    int f=0;
    cout<<sd<<"   ";
    for(int i=q.front;i<=q.rear;i++)
    {
        if(sd==q.q[i])
        {
            f=1;
            return i;
            
        }
            
    }
    if(f==0)
        return -1;
}