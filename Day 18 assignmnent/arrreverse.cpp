#include<iostream>
using namespace std;
void arrrev(int [],int);//array reverse function
int maxarr(int [],int);//Find max in array
int minarr(int [],int);//find min in aray
void maxmin(int [],int,int *,int *); //Finding max min in same function with pointer
int main()
{
    int a[10],n=6,i,max,min;
    
    for(i=0;i<n;i++)
    {
        a[i]=(i+n)*3;
        if(i==0)
        {
            max=a[i];
            min=a[i];
        }
    }
    cout<<endl<<"Original Array:";
    for(i=0;i<n;i++)
    {
        cout<<"\t"<<a[i];
    }
    arrrev(a,n);
    cout<<endl<<"Reverse Array:";
    
        for(i=0;i<n;i++)
    {
        cout<<"\t"<<a[i];
    }
    cout<<endl<<"Max element in array is:"<<maxarr(a,n);
    cout<<endl<<"Min element in array is:"<<minarr(a,n);
    //Calling MaxMin function with address
    cout<<endl<< endl<<"Finding max min in same function with pointer:";
    maxmin(a,n,&max,&min);
    cout<<endl<<"Max element in array is:"<<max;
    cout<<endl<<"Min element in array is:"<<min;
    return 0;
}
void arrrev(int a[],int n)
{
    cout<<"\n";
    int i,temp;
    for(int i=0;i<n/2;i++)
    {
        temp=a[i];
        a[i]=a[n-i-1];
        a[n-i-1]=temp;      
    }
}
int maxarr(int a[],int n)
{
    int max=0;
    for(int i=0;i<n;i++)
    {
        if(a[i]>max)
            max=a[i];
    }
    return max;
}
int minarr(int a[],int n)
{
    int min=a[0];
    for(int i=0;i<n;i++)
    {
        if(a[i]<min)
            min=a[i];
    }
    return min;
}
void maxmin(int a[],int n, int *max,int *min)
{
    for(int i=0;i<n;i++)
    {
        if(a[i]<*min)
            *min=a[i];
        if(a[i]>*max)
            *max=a[i];
    }
}