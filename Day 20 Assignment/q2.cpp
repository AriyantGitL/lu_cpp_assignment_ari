#include<iostream>
using namespace std;
void AreaPeri(int,int *,int *);
int main()
{
    int a,area=0,peri=0;
    cout<<"Enter the length of square:";
    cin>>a;
    AreaPeri(a,&area,&peri);
    cout<<endl<<"Area of the Sqaure:"<<area;
    cout<<endl<<"Perimeter of the Sqaure:"<<peri;
    return 0;
}
void AreaPeri(int a,int *area,int *peri)
{
    *area=a*a;
    *peri=4*a;
}