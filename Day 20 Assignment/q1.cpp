#include<iostream>
using namespace std;
void swaprev(int,int *,int *);
int main()
{
    int n,sum=0,rev=0;
    cout<<"ENter a number";
    cin>>n;
    swaprev(n,&sum,&rev);
    cout<<endl<<"sum of digit:"<<sum;
    cout<<endl<<"reverse of the number:"<<rev;
    return 0;
}
void swaprev(int n,int *sum,int *rev)
{
    int temp;
    while(n>0)
    {
        temp=n%10;
        *sum=*sum+temp;
        *rev=*rev*10+temp;
        n=n/10;
    }
}