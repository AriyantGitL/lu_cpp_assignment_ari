#include<iostream>
#include<iomanip>
using namespace std;

int main()
{
    int a[3][3],i,j,max=0,min=0;
    for(i=0;i<3;i++)
    {
        for(j=0;j<3;j++)
        {
            cout<<"Enter value for "<<i<<" , "<<j<<" position:";
            cin>>a[i][j];
            if(i==0 && j==0)
            {
                max=a[i][j];
                min=a[i][j];
            }
            if(a[i][j]>max)
                max=a[i][j];
            if(a[i][j]<min)
                min=a[i][j];
        }
    }
        for(i=0;i<3;i++)
        {
            for(j=0;j<3;j++)
            {
                cout<<setw(3)<<a[i][j];
            }
            cout<<endl;
        }
    cout<<"\nMax number from matrix is:"<<max;
    cout<<"\nMin number from matrix is:"<<min;
    return 0;
}